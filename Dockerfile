FROM rikorose/gcc-cmake:gcc-6 AS build

ENV ZOPFLI_VERSION=v1.0.2

# Static linking for portable binaries
ENV CFLAGS='-static -static-libstdc++ -static-libgcc' \
  CXXFLAGS='-static -static-libstdc++ -static-libgcc'

WORKDIR /source
RUN wget -O- https://github.com/google/zopfli/archive/master.tar.gz | tar -xz --strip=1 \
  && cmake . \
  && make VERBOSE=1

# ---
FROM alpine:3.8
COPY --from=build ["/source/zopfli", "/source/zopflipng", "/usr/bin/"]
